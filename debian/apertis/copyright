Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2016, Free Software
License: FSFAP

Files: build-aux/*
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: Expat

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2008-2011 Nokia <ivan.frade@nokia.com>
           2006 Jamie McCracken <jamiemcc@gnome.org>
           2014 Lanedo <martyn@lanedo.com>
License: GPL-2.0+

Files: debian/apparmor.d/*
Copyright: 2012-2016, Collabora Ltd.
License: GPL-2

Files: docs/reference/*
Copyright: 1994-2018, Free Software Foundation, Inc.
License: GPL-3+

Files: docs/tools/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: docs/tools/ttlresource2sgml.c
Copyright: 2015, Carlos Garnacho
License: GPL-2+

Files: docs/tools/ttlresource2sgml.h
Copyright: 2017, Red Hat, Inc
License: GPL-2+

Files: examples/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: m4/attributes.m4
Copyright: 2012, Lucas De Marchi <lucas.de.marchi@gmail.com>
 2006-2008, xine project
 2006-2008, Diego Pettenò <flameeyes@gmail.com>
License: GPL-2+

Files: m4/intltool.m4
Copyright: 2001, Eazel, Inc.
License: GPL-2+

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: GPL

Files: m4/pkg.m4
Copyright: 2012-2015, Dan Nicholson <dbn.lists@gmail.com>
 2004, Scott James Remnant <scott@netsplit.com>.
License: GPL

Files: src/gvdb/*
Copyright: 2010, Codethink Limited
License: LGPL-2

Files: src/libtracker-bus/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-bus/tracker-namespace.c
 src/libtracker-bus/tracker-namespace.vala
Copyright: 2015, Collabora Ltd.
License: LGPL-2.1+

Files: src/libtracker-common/libtracker-common.vapi
Copyright: 2008-2011, Nokia
License: LGPL-2.1+

Files: src/libtracker-common/tracker-common.h
 src/libtracker-common/tracker-dbus.c
 src/libtracker-common/tracker-dbus.h
 src/libtracker-common/tracker-enums.h
 src/libtracker-common/tracker-file-utils.h
 src/libtracker-common/tracker-locale.c
 src/libtracker-common/tracker-locale.h
 src/libtracker-common/tracker-log.c
 src/libtracker-common/tracker-log.h
 src/libtracker-common/tracker-parser-utils.c
 src/libtracker-common/tracker-parser-utils.h
 src/libtracker-common/tracker-sched.c
 src/libtracker-common/tracker-sched.h
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-common/tracker-date-time.c
 src/libtracker-common/tracker-date-time.h
 src/libtracker-common/tracker-file-utils.c
 src/libtracker-common/tracker-language.c
 src/libtracker-common/tracker-language.h
 src/libtracker-common/tracker-parser-libicu.c
 src/libtracker-common/tracker-parser-libunistring.c
 src/libtracker-common/tracker-parser.h
 src/libtracker-common/tracker-type-utils.c
 src/libtracker-common/tracker-type-utils.h
 src/libtracker-common/tracker-utils.c
 src/libtracker-common/tracker-utils.h
Copyright: 2008-2010, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-common/tracker-domain-ontology.c
 src/libtracker-common/tracker-domain-ontology.h
Copyright: 2017, Red Hat, Inc.
License: LGPL-2.1+

Files: src/libtracker-common/tracker-ioprio.c
Copyright: 2006, Jamie McCracken <jamiemcc@gnome.org>
 2006, Anders Aagaard
 2005, Novell, Inc.
License: Expat

Files: src/libtracker-common/tracker-ioprio.h
Copyright: 2008, Nokia <ivan.frade@nokia.com>
 2006, Anders Aagaard
License: LGPL-2.1+

Files: src/libtracker-control/*
Copyright: 2014, 2016, Carlos Garnacho <carlosg@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-control/tracker-miner-manager.c
Copyright: 2015, 2017, Red Hat, Inc.
 2009, 2010, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-control/tracker-miner-manager.h
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-data/libtracker-data.vapi
 src/libtracker-data/tracker-db-backup.c
 src/libtracker-data/tracker-db-backup.h
 src/libtracker-data/tracker-sparql-expression.c
 src/libtracker-data/tracker-sparql-expression.vala
 src/libtracker-data/tracker-sparql-pattern.c
 src/libtracker-data/tracker-sparql-pattern.vala
 src/libtracker-data/tracker-sparql-query.c
 src/libtracker-data/tracker-sparql-query.vala
 src/libtracker-data/tracker-sparql-scanner.c
 src/libtracker-data/tracker-sparql-scanner.vala
 src/libtracker-data/tracker-turtle-reader.c
 src/libtracker-data/tracker-turtle-reader.vala
Copyright: 2008-2011, Nokia
License: LGPL-2.1+

Files: src/libtracker-data/org.freedesktop.Tracker.DB.gschema.xml.in
 src/libtracker-data/tracker-collation.h
 src/libtracker-data/tracker-data.h
 src/libtracker-data/tracker-db-config.h
 src/libtracker-data/tracker-db-interface-sqlite.c
 src/libtracker-data/tracker-db-interface-sqlite.h
 src/libtracker-data/tracker-db-interface.c
 src/libtracker-data/tracker-db-interface.h
 src/libtracker-data/tracker-db-journal.c
 src/libtracker-data/tracker-db-journal.h
 src/libtracker-data/tracker-db-manager.c
 src/libtracker-data/tracker-db-manager.h
 src/libtracker-data/tracker-namespace.c
 src/libtracker-data/tracker-namespace.h
 src/libtracker-data/tracker-ontology.c
 src/libtracker-data/tracker-ontology.h
 src/libtracker-data/tracker-property.c
 src/libtracker-data/tracker-property.h
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-data/tracker-class.c
 src/libtracker-data/tracker-class.h
 src/libtracker-data/tracker-data-backup.c
 src/libtracker-data/tracker-data-backup.h
 src/libtracker-data/tracker-data-update.c
 src/libtracker-data/tracker-data-update.h
 src/libtracker-data/tracker-ontologies.c
 src/libtracker-data/tracker-ontologies.h
Copyright: 2008-2010, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-data/tracker-collation.c
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: src/libtracker-data/tracker-data-manager.c
 src/libtracker-data/tracker-data-manager.h
 src/libtracker-data/tracker-data-query.c
 src/libtracker-data/tracker-data-query.h
Copyright: 2008, Nokia <ivan.frade@nokia.com>
 2007, Jason Kivlighn <jkivlighn@gmail.com>
 2007, Creative Commons <http:creativecommons.org>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-data/tracker-db-config.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2009, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-data/tracker-vala-namespace.c
 src/libtracker-data/tracker-vala-namespace.vala
Copyright: 2015, Collabora Ltd.
License: LGPL-2.1+

Files: src/libtracker-direct/*
Copyright: 2015, 2017, Red Hat, Inc.
 2009, 2010, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-fts/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-fts/tracker-fts-config.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2009, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-crawler.c
 src/libtracker-miner/tracker-crawler.h
 src/libtracker-miner/tracker-file-notifier.c
 src/libtracker-miner/tracker-file-notifier.h
 src/libtracker-miner/tracker-file-system.c
 src/libtracker-miner/tracker-file-system.h
 src/libtracker-miner/tracker-indexing-tree.c
 src/libtracker-miner/tracker-indexing-tree.h
 src/libtracker-miner/tracker-miner-enums.h
 src/libtracker-miner/tracker-miner-fs.c
 src/libtracker-miner/tracker-miner-fs.h
 src/libtracker-miner/tracker-miner-object.c
 src/libtracker-miner/tracker-miner-object.h
 src/libtracker-miner/tracker-miner.h
 src/libtracker-miner/tracker-monitor.c
 src/libtracker-miner/tracker-monitor.h
 src/libtracker-miner/tracker-priority-queue.c
 src/libtracker-miner/tracker-priority-queue.h
 src/libtracker-miner/tracker-sparql-buffer.c
 src/libtracker-miner/tracker-sparql-buffer.h
 src/libtracker-miner/tracker-task-pool.c
 src/libtracker-miner/tracker-task-pool.h
 src/libtracker-miner/tracker-utils.c
 src/libtracker-miner/tracker-utils.h
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-data-provider.c
 src/libtracker-miner/tracker-data-provider.h
 src/libtracker-miner/tracker-file-data-provider.c
 src/libtracker-miner/tracker-file-data-provider.h
Copyright: 2014, Softathome <contact@softathome.com>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-decorator-fs.c
 src/libtracker-miner/tracker-decorator-fs.h
 src/libtracker-miner/tracker-decorator-private.h
 src/libtracker-miner/tracker-decorator.c
 src/libtracker-miner/tracker-decorator.h
Copyright: 2014, 2016, Carlos Garnacho <carlosg@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-miner-online.c
Copyright: 2014, Carlos Garnacho <carlosg@gnome.org>
 2009-2014, Adrien Bustany <abustany@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-miner-online.h
Copyright: 2009, Adrien Bustany <abustany@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-miner/tracker-miner-proxy.c
 src/libtracker-miner/tracker-miner-proxy.h
Copyright: 2017, Red Hat, Inc.
License: LGPL-2.1+

Files: src/libtracker-remote/*
Copyright: 2014, 2016, Carlos Garnacho <carlosg@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-sparql-backend/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-sparql/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/libtracker-sparql/libtracker-sparql-intermediate-c.vapi
Copyright: 2017, - Red Hat Inc
License: LGPL-2.1+

Files: src/libtracker-sparql/tracker-namespace-manager.c
 src/libtracker-sparql/tracker-namespace-manager.h
 src/libtracker-sparql/tracker-resource.c
 src/libtracker-sparql/tracker-resource.h
 src/libtracker-sparql/tracker-uri.h
Copyright: 2016, 2017, Sam Thursfield <sam@afuera.me.uk>
License: LGPL-2.1+

Files: src/libtracker-sparql/tracker-namespace.c
 src/libtracker-sparql/tracker-namespace.vala
Copyright: 2015, Collabora Ltd.
License: LGPL-2.1+

Files: src/libtracker-sparql/tracker-notifier.c
 src/libtracker-sparql/tracker-notifier.h
Copyright: 2016-2018, Red Hat Inc.
License: LGPL-2.1+

Files: src/libtracker-sparql/tracker-ontologies.h
 src/libtracker-sparql/tracker-version.h
Copyright: 2008-2010, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2.1+

Files: src/libtracker-sparql/tracker-uri.c
Copyright: 2016, Sam Thursfield <sam@afuera.me.uk>
 2010, Codeminded BVBA <abustany@gnome.org>
 2008-2010, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/tracker-store/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: src/tracker-store/org.freedesktop.Tracker.Store.gschema.xml.in
 src/tracker-store/org.freedesktop.Tracker.gschema.xml.in
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: src/tracker-store/tracker-backup.c
 src/tracker-store/tracker-backup.vala
 src/tracker-store/tracker-dbus.c
 src/tracker-store/tracker-dbus.vala
 src/tracker-store/tracker-main.c
 src/tracker-store/tracker-main.vala
 src/tracker-store/tracker-resources.c
 src/tracker-store/tracker-resources.vala
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: GPL-2+

Files: src/tracker-store/tracker-config.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2008-2010, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: src/tracker-store/tracker-steroids.c
 src/tracker-store/tracker-steroids.vala
Copyright: 2011, Nokia <ivan.frade@nokia.com>
 2010, Codeminded BVBA <abustany@gnome.org>
License: LGPL-2.1+

Files: src/tracker/*
Copyright: 2014, Lanedo <martyn@lanedo.com>
License: GPL-2+

Files: src/tracker/tracker-config.c
 src/tracker/tracker-daemon.c
 src/tracker/tracker-dbus.c
 src/tracker/tracker-process.c
 src/tracker/tracker-process.h
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2008-2010, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: src/tracker/tracker-config.h
Copyright: 2014, Nokia <ivan.frade@nokia.com>
 2014, Lanedo <martyn@lanedo.com>
License: GPL-2+

Files: src/tracker/tracker-extract.c
 src/tracker/tracker-extract.h
Copyright: 2015-2017, Sam Thursfield <sam@afuera.me.uk>
License: GPL-2+

Files: src/tracker/tracker-info.c
Copyright: 2014, SoftAtHome <contact@softathome.com>
 2014, Lanedo <martyn@lanedo.com>
 2008-2010, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2+

Files: src/tracker/tracker-search.c
Copyright: 2014, SoftAtHome <contact@softathome.com>
 2014, Lanedo <martyn@lanedo.com>
 2009, Nokia <ivan.frade@nokia.com>
License: LGPL-2+

Files: src/tracker/tracker-sparql.c
Copyright: 2014, Softathome <philippe.judge@softathome.com>
 2014, Lanedo <martyn@lanedo.com>
 2009, Nokia <ivan.frade@nokia.com>
License: LGPL-2+

Files: src/tracker/tracker-sql.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2010, Nokia
License: GPL-2+

Files: src/tracker/tracker-status.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2008, Nokia <ivan.frade@nokia.com>
 2006, Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2+

Files: src/tracker/tracker-tag.c
Copyright: 2014, Lanedo <martyn@lanedo.com>
 2009, Nokia <ivan.frade@nokia.com>
License: LGPL-2+

Files: tap-driver.sh
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+

Files: tests/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: tests/functional-tests/common/utils/*
Copyright: 2010, Nokia <jean-luc.lamadon@nokia.com>
License: GPL-2+

Files: tests/functional-tests/common/utils/storetest.py
Copyright: 2018, Sam Thursfield <sam@afuera.me.uk>
 2010, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: tests/functional-tests/ipc/*
Copyright: 2008, 2010, Nokia <ivan.frade@nokia.com>
License: LGPL-2+

Files: tests/functional-tests/ipc/test-bus-query-cancellation.c
Copyright: 2014, Red Hat, Inc.
License: GPL-2+

Files: tests/functional-tests/ipc/test-update-array-performance.c
Copyright: 2010, Codeminded BVBA <abustany@gnome.org>
License: GPL-2+

Files: tests/libtracker-data/tracker-backup-test.c
 tests/libtracker-data/tracker-crc32-test.c
 tests/libtracker-data/tracker-db-journal-test.c
 tests/libtracker-data/tracker-ontology-change-test.c
 tests/libtracker-data/tracker-ontology-test.c
 tests/libtracker-data/tracker-sparql-blank-test.c
 tests/libtracker-data/tracker-sparql-test.c
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: tests/libtracker-fts/tracker-fts-test.c
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: tests/libtracker-miner/tracker-file-enumerator-test.c
Copyright: 2014, Softathome <contact@softathome.com>
License: GPL-2+

Files: tests/libtracker-miner/tracker-miner-mock.c
 tests/libtracker-miner/tracker-miner-mock.vala
Copyright: 2010, Nokia
License: GPL-2+

Files: tests/libtracker-miner/tracker-task-pool-test.c
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: LGPL-2.1+

Files: tests/libtracker-sparql/tracker-resource-test.c
Copyright: 2015-2017, Sam Thursfield <sam@afuera.me.uk>
License: GPL-2+

Files: tests/tracker-steroids/tracker-test.c
Copyright: 2010, Codeminded BVBA <abustany@gnome.org>
License: GPL-2+

Files: utils/*
Copyright: 2010, Nokia
License: GPL-2+

Files: utils/data-generators/*
Copyright: 2007, Chris Moffitt
License: GPL-2+

Files: utils/g-ir-merge/*
Copyright: 2015-2017, Sam Thursfield <sam@afuera.me.uk>
License: GPL-2+

Files: utils/mtp/*
Copyright: 2008, 2010, Nokia <ivan.frade@nokia.com>
License: LGPL-2+

Files: utils/ontology/*
Copyright: 2008-2011, Nokia <ivan.frade@nokia.com>
License: GPL-2+

Files: utils/sandbox/*
Copyright: 2016, Sam Thursfield <sam@afuera.me.uk>
 2012, Sam Thursfield <sam.thursfield@codethink.co.uk>
 2012, 2013, Martyn Russell <martyn@lanedo.com>
License: GPL-2+

Files: * ChangeLog.pre-0-6-93 NEWS docs/design/tracker-miner-fs.dia docs/design/tracker-store.dia docs/reference/libtracker-control/html/* docs/reference/libtracker-miner/html/* docs/reference/libtracker-sparql/html/* docs/reference/libtracker-sparql/html/nao-ontology.html docs/reference/ontology/html/* docs/reference/ontology/html/nie-ontology.html docs/tools/Makefile.in examples/Makefile.in examples/libtracker-miner/Makefile.in examples/libtracker-sparql/Makefile.in examples/rss-reader/Makefile.in m4/* m4/introspection.m4 m4/ltversion.m4 m4/sqlite-threadsafe.m4 src/ontologies/10-xsd.description src/ontologies/31-nao.description src/ontologies/nepomuk/* src/ontologies/nepomuk/35-ncal.description src/tracker-store/Makefile.in src/tracker/Makefile.in tests/Makefile.in tests/common/Makefile.in tests/functional-tests/Makefile.in tests/functional-tests/common/* tests/functional-tests/common/utils/Makefile.in tests/functional-tests/common/utils/expectedFailure.py tests/functional-tests/ipc/Makefile.in tests/functional-tests/test-ontologies/* tests/functional-tests/ttl/* tests/functional-tests/unittest2/* tests/functional-tests/unittest2/__init__.py tests/gvdb/* tests/libtracker-common/Makefile.in tests/libtracker-data/* tests/libtracker-data/dawg-testcases tests/libtracker-fts/* tests/libtracker-miner/Makefile.in tests/libtracker-sparql/Makefile.in tests/tracker-steroids/* utils/Makefile.in utils/data-generators/Makefile.in utils/data-generators/cc/Makefile.in utils/mtp/Makefile.in utils/ontology/Makefile.in utils/sandbox/Makefile.in utils/tracker-resdump/Makefile.in
Copyright: 2008-2011 Nokia <ivan.frade@nokia.com>
 2006 Jamie McCracken <jamiemcc@gnome.org>
 2014 Lanedo <martyn@lanedo.com>
License: GPL-2.0+

Files: src/gvdb/Makefile.in
Copyright: 2010 Codethink Limited
License: LGPL-2.0+

Files: src/libtracker-bus/Makefile.in src/libtracker-control/Makefile.in src/libtracker-data/tracker-crc32.c src/libtracker-data/tracker-crc32.h src/libtracker-data/tracker-crc32.c src/libtracker-data/tracker-crc32.h src/libtracker-direct/Makefile.in src/libtracker-fts/Makefile.in src/libtracker-remote/Makefile.in src/libtracker-sparql-backend/Makefile.in src/libtracker-sparql/Makefile.in
Copyright: 2008-2011 Nokia <ivan.frade@nokia.com>
 2006 Jamie McCracken <jamiemcc@gnome.org>
License: LGPL-2.1+
