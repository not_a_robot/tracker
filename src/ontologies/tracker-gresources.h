#ifndef __RESOURCE_tracker_gresources_H__
#define __RESOURCE_tracker_gresources_H__

#include <gio/gio.h>

extern GResource *tracker_gresources_get_resource (void);
#endif
