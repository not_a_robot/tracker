/* test-shared-query.c generated by valac 0.41.90.2-8c9f4-dirty, the Vala compiler
 * generated from test-shared-query.vala, do not modify */



#include <glib.h>
#include <glib-object.h>
#include "libtracker-sparql/tracker-sparql.h"
#include <stdlib.h>
#include <string.h>
#include <gio/gio.h>


#define TYPE_TEST_APP (test_app_get_type ())
#define TEST_APP(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_TEST_APP, TestApp))
#define TEST_APP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_TEST_APP, TestAppClass))
#define IS_TEST_APP(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_TEST_APP))
#define IS_TEST_APP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_TEST_APP))
#define TEST_APP_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_TEST_APP, TestAppClass))

typedef struct _TestApp TestApp;
typedef struct _TestAppClass TestAppClass;
typedef struct _TestAppPrivate TestAppPrivate;
enum  {
	TEST_APP_0_PROPERTY,
	TEST_APP_NUM_PROPERTIES
};
static GParamSpec* test_app_properties[TEST_APP_NUM_PROPERTIES];
#define _g_main_loop_unref0(var) ((var == NULL) ? NULL : (var = (g_main_loop_unref (var), NULL)))
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_free0(var) (var = (g_free (var), NULL))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
typedef struct _TestAppTestQueryAsyncData TestAppTestQueryAsyncData;
typedef struct _TestAppDoAsyncTestsData TestAppDoAsyncTestsData;

struct _TestApp {
	GObject parent_instance;
	TestAppPrivate * priv;
};

struct _TestAppClass {
	GObjectClass parent_class;
};

struct _TestAppPrivate {
	GMainLoop* loop;
	TrackerSparqlConnection* con;
	gint res;
};

struct _TestAppTestQueryAsyncData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GTask* _async_result;
	TestApp* self;
	TrackerSparqlCursor* cursor;
	TrackerSparqlCursor* _tmp0_;
	TrackerSparqlConnection* _tmp1_;
	TrackerSparqlCursor* _tmp2_;
	TrackerSparqlCursor* _tmp3_;
	GError* e;
	GError* _tmp4_;
	const gchar* _tmp5_;
	TrackerSparqlCursor* _tmp6_;
	gint _tmp7_;
	TrackerSparqlCursor* _tmp8_;
	TrackerSparqlCursor* _tmp9_;
	GError * _inner_error_;
};

struct _TestAppDoAsyncTestsData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GTask* _async_result;
	TestApp* self;
	GMainLoop* _tmp0_;
};


static gint TestApp_private_offset;
static gpointer test_app_parent_class = NULL;

GType test_app_get_type (void) G_GNUC_CONST;
TestApp* test_app_new (TrackerSparqlConnection* connection);
TestApp* test_app_construct (GType object_type,
                             TrackerSparqlConnection* connection);
static gchar* test_app_type_to_string (TestApp* self,
                                TrackerSparqlValueType type);
static gint test_app_iter_cursor (TestApp* self,
                           TrackerSparqlCursor* cursor);
static void test_app_test_query (TestApp* self);
static void test_app_test_query_async_data_free (gpointer _data);
static void test_app_test_query_async (TestApp* self,
                                GAsyncReadyCallback _callback_,
                                gpointer _user_data_);
static void test_app_test_query_finish (TestApp* self,
                                 GAsyncResult* _res_);
static gboolean test_app_test_query_async_co (TestAppTestQueryAsyncData* _data_);
static void test_app_test_query_async_ready (GObject* source_object,
                                      GAsyncResult* _res_,
                                      gpointer _user_data_);
static void test_app_do_sync_tests (TestApp* self);
static void test_app_do_async_tests_data_free (gpointer _data);
static void test_app_do_async_tests (TestApp* self,
                              GAsyncReadyCallback _callback_,
                              gpointer _user_data_);
static void test_app_do_async_tests_finish (TestApp* self,
                                     GAsyncResult* _res_);
static gboolean test_app_do_async_tests_co (TestAppDoAsyncTestsData* _data_);
static void test_app_do_async_tests_ready (GObject* source_object,
                                    GAsyncResult* _res_,
                                    gpointer _user_data_);
static gboolean test_app_in_mainloop (TestApp* self);
gint test_app_run (TestApp* self);
static gboolean _test_app_in_mainloop_gsource_func (gpointer self);
static void test_app_finalize (GObject * obj);


static inline gpointer
test_app_get_instance_private (TestApp* self)
{
	return G_STRUCT_MEMBER_P (self, TestApp_private_offset);
}


static gpointer
_g_object_ref0 (gpointer self)
{
#line 10 "test-shared-query.vala"
	return self ? g_object_ref (self) : NULL;
#line 134 "test-shared-query.c"
}


TestApp*
test_app_construct (GType object_type,
                    TrackerSparqlConnection* connection)
{
	TestApp * self = NULL;
	TrackerSparqlConnection* _tmp0_;
#line 9 "test-shared-query.vala"
	g_return_val_if_fail (connection != NULL, NULL);
#line 9 "test-shared-query.vala"
	self = (TestApp*) g_object_new (object_type, NULL);
#line 10 "test-shared-query.vala"
	_tmp0_ = _g_object_ref0 (connection);
#line 10 "test-shared-query.vala"
	_g_object_unref0 (self->priv->con);
#line 10 "test-shared-query.vala"
	self->priv->con = _tmp0_;
#line 9 "test-shared-query.vala"
	return self;
#line 156 "test-shared-query.c"
}


TestApp*
test_app_new (TrackerSparqlConnection* connection)
{
#line 9 "test-shared-query.vala"
	return test_app_construct (TYPE_TEST_APP, connection);
#line 165 "test-shared-query.c"
}


static gchar*
test_app_type_to_string (TestApp* self,
                         TrackerSparqlValueType type)
{
	gchar* result = NULL;
	gchar* _tmp8_;
#line 13 "test-shared-query.vala"
	g_return_val_if_fail (self != NULL, NULL);
#line 14 "test-shared-query.vala"
	switch (type) {
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_UNBOUND:
#line 181 "test-shared-query.c"
		{
			gchar* _tmp0_;
#line 16 "test-shared-query.vala"
			_tmp0_ = g_strdup ("unbound");
#line 16 "test-shared-query.vala"
			result = _tmp0_;
#line 16 "test-shared-query.vala"
			return result;
#line 190 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_URI:
#line 194 "test-shared-query.c"
		{
			gchar* _tmp1_;
#line 18 "test-shared-query.vala"
			_tmp1_ = g_strdup ("uri");
#line 18 "test-shared-query.vala"
			result = _tmp1_;
#line 18 "test-shared-query.vala"
			return result;
#line 203 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_STRING:
#line 207 "test-shared-query.c"
		{
			gchar* _tmp2_;
#line 20 "test-shared-query.vala"
			_tmp2_ = g_strdup ("string");
#line 20 "test-shared-query.vala"
			result = _tmp2_;
#line 20 "test-shared-query.vala"
			return result;
#line 216 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_INTEGER:
#line 220 "test-shared-query.c"
		{
			gchar* _tmp3_;
#line 22 "test-shared-query.vala"
			_tmp3_ = g_strdup ("integer");
#line 22 "test-shared-query.vala"
			result = _tmp3_;
#line 22 "test-shared-query.vala"
			return result;
#line 229 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_DOUBLE:
#line 233 "test-shared-query.c"
		{
			gchar* _tmp4_;
#line 24 "test-shared-query.vala"
			_tmp4_ = g_strdup ("double");
#line 24 "test-shared-query.vala"
			result = _tmp4_;
#line 24 "test-shared-query.vala"
			return result;
#line 242 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_DATETIME:
#line 246 "test-shared-query.c"
		{
			gchar* _tmp5_;
#line 26 "test-shared-query.vala"
			_tmp5_ = g_strdup ("datetime");
#line 26 "test-shared-query.vala"
			result = _tmp5_;
#line 26 "test-shared-query.vala"
			return result;
#line 255 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_BLANK_NODE:
#line 259 "test-shared-query.c"
		{
			gchar* _tmp6_;
#line 28 "test-shared-query.vala"
			_tmp6_ = g_strdup ("blank-node");
#line 28 "test-shared-query.vala"
			result = _tmp6_;
#line 28 "test-shared-query.vala"
			return result;
#line 268 "test-shared-query.c"
		}
#line 14 "test-shared-query.vala"
		case TRACKER_SPARQL_VALUE_TYPE_BOOLEAN:
#line 272 "test-shared-query.c"
		{
			gchar* _tmp7_;
#line 30 "test-shared-query.vala"
			_tmp7_ = g_strdup ("boolean");
#line 30 "test-shared-query.vala"
			result = _tmp7_;
#line 30 "test-shared-query.vala"
			return result;
#line 281 "test-shared-query.c"
		}
		default:
		{
#line 32 "test-shared-query.vala"
			break;
#line 287 "test-shared-query.c"
		}
	}
#line 34 "test-shared-query.vala"
	_tmp8_ = g_strdup ("unknown");
#line 34 "test-shared-query.vala"
	result = _tmp8_;
#line 34 "test-shared-query.vala"
	return result;
#line 296 "test-shared-query.c"
}


static gint
test_app_iter_cursor (TestApp* self,
                      TrackerSparqlCursor* cursor)
{
	gint result = 0;
	gint i = 0;
	GError * _inner_error_ = NULL;
#line 37 "test-shared-query.vala"
	g_return_val_if_fail (self != NULL, 0);
#line 37 "test-shared-query.vala"
	g_return_val_if_fail (cursor != NULL, 0);
#line 311 "test-shared-query.c"
	{
		gint _tmp7_;
		gint _tmp8_;
		{
			gboolean _tmp0_ = FALSE;
#line 41 "test-shared-query.vala"
			i = 0;
#line 41 "test-shared-query.vala"
			_tmp0_ = TRUE;
#line 41 "test-shared-query.vala"
			while (TRUE) {
#line 323 "test-shared-query.c"
				gint _tmp2_;
				gint _tmp3_;
				gint _tmp4_;
				gint _tmp5_;
				const gchar* _tmp6_;
#line 41 "test-shared-query.vala"
				if (!_tmp0_) {
#line 331 "test-shared-query.c"
					gint _tmp1_;
#line 41 "test-shared-query.vala"
					_tmp1_ = i;
#line 41 "test-shared-query.vala"
					i = _tmp1_ + 1;
#line 337 "test-shared-query.c"
				}
#line 41 "test-shared-query.vala"
				_tmp0_ = FALSE;
#line 41 "test-shared-query.vala"
				_tmp2_ = i;
#line 41 "test-shared-query.vala"
				_tmp3_ = tracker_sparql_cursor_get_n_columns (cursor);
#line 41 "test-shared-query.vala"
				_tmp4_ = _tmp3_;
#line 41 "test-shared-query.vala"
				if (!(_tmp2_ < _tmp4_)) {
#line 41 "test-shared-query.vala"
					break;
#line 351 "test-shared-query.c"
				}
#line 42 "test-shared-query.vala"
				_tmp5_ = i;
#line 42 "test-shared-query.vala"
				_tmp6_ = tracker_sparql_cursor_get_variable_name (cursor, _tmp5_);
#line 42 "test-shared-query.vala"
				g_print ("| %s ", _tmp6_);
#line 359 "test-shared-query.c"
			}
		}
#line 44 "test-shared-query.vala"
		_tmp7_ = tracker_sparql_cursor_get_n_columns (cursor);
#line 44 "test-shared-query.vala"
		_tmp8_ = _tmp7_;
#line 44 "test-shared-query.vala"
		g_print ("| -> %d columns\n", _tmp8_);
#line 46 "test-shared-query.vala"
		while (TRUE) {
#line 370 "test-shared-query.c"
			gboolean _tmp9_ = FALSE;
#line 46 "test-shared-query.vala"
			_tmp9_ = tracker_sparql_cursor_next (cursor, NULL, &_inner_error_);
#line 46 "test-shared-query.vala"
			if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 376 "test-shared-query.c"
				goto __catch0_g_error;
			}
#line 46 "test-shared-query.vala"
			if (!_tmp9_) {
#line 46 "test-shared-query.vala"
				break;
#line 383 "test-shared-query.c"
			}
			{
				gboolean _tmp10_ = FALSE;
#line 47 "test-shared-query.vala"
				i = 0;
#line 47 "test-shared-query.vala"
				_tmp10_ = TRUE;
#line 47 "test-shared-query.vala"
				while (TRUE) {
#line 393 "test-shared-query.c"
					gint _tmp12_;
					gint _tmp13_;
					gint _tmp14_;
					const gchar* _tmp15_ = NULL;
					gint _tmp16_;
					gint _tmp17_;
					const gchar* _tmp18_;
					gint _tmp19_;
					gchar* _tmp20_;
					gchar* _tmp21_;
#line 47 "test-shared-query.vala"
					if (!_tmp10_) {
#line 406 "test-shared-query.c"
						gint _tmp11_;
#line 47 "test-shared-query.vala"
						_tmp11_ = i;
#line 47 "test-shared-query.vala"
						i = _tmp11_ + 1;
#line 412 "test-shared-query.c"
					}
#line 47 "test-shared-query.vala"
					_tmp10_ = FALSE;
#line 47 "test-shared-query.vala"
					_tmp12_ = i;
#line 47 "test-shared-query.vala"
					_tmp13_ = tracker_sparql_cursor_get_n_columns (cursor);
#line 47 "test-shared-query.vala"
					_tmp14_ = _tmp13_;
#line 47 "test-shared-query.vala"
					if (!(_tmp12_ < _tmp14_)) {
#line 47 "test-shared-query.vala"
						break;
#line 426 "test-shared-query.c"
					}
#line 48 "test-shared-query.vala"
					_tmp16_ = i;
#line 48 "test-shared-query.vala"
					if (_tmp16_ != 0) {
#line 48 "test-shared-query.vala"
						_tmp15_ = ",";
#line 434 "test-shared-query.c"
					} else {
#line 48 "test-shared-query.vala"
						_tmp15_ = "";
#line 438 "test-shared-query.c"
					}
#line 48 "test-shared-query.vala"
					_tmp17_ = i;
#line 48 "test-shared-query.vala"
					_tmp18_ = tracker_sparql_cursor_get_string (cursor, _tmp17_, NULL);
#line 48 "test-shared-query.vala"
					_tmp19_ = i;
#line 48 "test-shared-query.vala"
					_tmp20_ = test_app_type_to_string (self, tracker_sparql_cursor_get_value_type (cursor, _tmp19_));
#line 48 "test-shared-query.vala"
					_tmp21_ = _tmp20_;
#line 48 "test-shared-query.vala"
					g_print ("%s%s a %s", _tmp15_, _tmp18_, _tmp21_);
#line 48 "test-shared-query.vala"
					_g_free0 (_tmp21_);
#line 454 "test-shared-query.c"
				}
			}
#line 53 "test-shared-query.vala"
			g_print ("\n");
#line 459 "test-shared-query.c"
		}
	}
	goto __finally0;
	__catch0_g_error:
	{
		GError* e = NULL;
		GError* _tmp22_;
		const gchar* _tmp23_;
#line 40 "test-shared-query.vala"
		e = _inner_error_;
#line 40 "test-shared-query.vala"
		_inner_error_ = NULL;
#line 56 "test-shared-query.vala"
		_tmp22_ = e;
#line 56 "test-shared-query.vala"
		_tmp23_ = _tmp22_->message;
#line 56 "test-shared-query.vala"
		g_warning ("test-shared-query.vala:56: Couldn't iterate query results: %s", _tmp23_);
#line 57 "test-shared-query.vala"
		result = -1;
#line 57 "test-shared-query.vala"
		_g_error_free0 (e);
#line 57 "test-shared-query.vala"
		return result;
#line 484 "test-shared-query.c"
	}
	__finally0:
#line 40 "test-shared-query.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 489 "test-shared-query.c"
		gint _tmp24_ = -1;
#line 40 "test-shared-query.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 40 "test-shared-query.vala"
		g_clear_error (&_inner_error_);
#line 40 "test-shared-query.vala"
		return _tmp24_;
#line 497 "test-shared-query.c"
	}
#line 60 "test-shared-query.vala"
	result = 0;
#line 60 "test-shared-query.vala"
	return result;
#line 503 "test-shared-query.c"
}


static void
test_app_test_query (TestApp* self)
{
	TrackerSparqlCursor* cursor = NULL;
	TrackerSparqlCursor* _tmp6_;
	gint _tmp7_;
	TrackerSparqlCursor* _tmp8_;
	TrackerSparqlCursor* _tmp9_;
	GError * _inner_error_ = NULL;
#line 63 "test-shared-query.vala"
	g_return_if_fail (self != NULL);
#line 66 "test-shared-query.vala"
	g_print ("Sync test\n");
#line 520 "test-shared-query.c"
	{
		TrackerSparqlCursor* _tmp0_ = NULL;
		TrackerSparqlConnection* _tmp1_;
		TrackerSparqlCursor* _tmp2_;
		TrackerSparqlCursor* _tmp3_;
#line 68 "test-shared-query.vala"
		_tmp1_ = self->priv->con;
#line 68 "test-shared-query.vala"
		_tmp2_ = tracker_sparql_connection_query (_tmp1_, "SELECT ?u WHERE { ?u a rdfs:Class }", NULL, &_inner_error_);
#line 68 "test-shared-query.vala"
		_tmp0_ = _tmp2_;
#line 68 "test-shared-query.vala"
		if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 534 "test-shared-query.c"
			goto __catch1_g_error;
		}
#line 68 "test-shared-query.vala"
		_tmp3_ = _tmp0_;
#line 68 "test-shared-query.vala"
		_tmp0_ = NULL;
#line 68 "test-shared-query.vala"
		_g_object_unref0 (cursor);
#line 68 "test-shared-query.vala"
		cursor = _tmp3_;
#line 67 "test-shared-query.vala"
		_g_object_unref0 (_tmp0_);
#line 547 "test-shared-query.c"
	}
	goto __finally1;
	__catch1_g_error:
	{
		GError* e = NULL;
		GError* _tmp4_;
		const gchar* _tmp5_;
#line 67 "test-shared-query.vala"
		e = _inner_error_;
#line 67 "test-shared-query.vala"
		_inner_error_ = NULL;
#line 70 "test-shared-query.vala"
		_tmp4_ = e;
#line 70 "test-shared-query.vala"
		_tmp5_ = _tmp4_->message;
#line 70 "test-shared-query.vala"
		g_warning ("test-shared-query.vala:70: Couldn't perform query: %s", _tmp5_);
#line 71 "test-shared-query.vala"
		self->priv->res = -1;
#line 72 "test-shared-query.vala"
		_g_error_free0 (e);
#line 72 "test-shared-query.vala"
		_g_object_unref0 (cursor);
#line 72 "test-shared-query.vala"
		return;
#line 573 "test-shared-query.c"
	}
	__finally1:
#line 67 "test-shared-query.vala"
	if (G_UNLIKELY (_inner_error_ != NULL)) {
#line 67 "test-shared-query.vala"
		_g_object_unref0 (cursor);
#line 67 "test-shared-query.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _inner_error_->message, g_quark_to_string (_inner_error_->domain), _inner_error_->code);
#line 67 "test-shared-query.vala"
		g_clear_error (&_inner_error_);
#line 67 "test-shared-query.vala"
		return;
#line 586 "test-shared-query.c"
	}
#line 75 "test-shared-query.vala"
	_tmp6_ = cursor;
#line 75 "test-shared-query.vala"
	self->priv->res = test_app_iter_cursor (self, _tmp6_);
#line 77 "test-shared-query.vala"
	_tmp7_ = self->priv->res;
#line 77 "test-shared-query.vala"
	if (_tmp7_ == -1) {
#line 78 "test-shared-query.vala"
		_g_object_unref0 (cursor);
#line 78 "test-shared-query.vala"
		return;
#line 600 "test-shared-query.c"
	}
#line 80 "test-shared-query.vala"
	g_print ("\nRewinding\n");
#line 81 "test-shared-query.vala"
	_tmp8_ = cursor;
#line 81 "test-shared-query.vala"
	tracker_sparql_cursor_rewind (_tmp8_);
#line 83 "test-shared-query.vala"
	g_print ("\nSecond run\n");
#line 84 "test-shared-query.vala"
	_tmp9_ = cursor;
#line 84 "test-shared-query.vala"
	self->priv->res = test_app_iter_cursor (self, _tmp9_);
#line 63 "test-shared-query.vala"
	_g_object_unref0 (cursor);
#line 616 "test-shared-query.c"
}


static void
test_app_test_query_async_data_free (gpointer _data)
{
	TestAppTestQueryAsyncData* _data_;
	_data_ = _data;
#line 4 "test-shared-query.vala"
	_g_object_unref0 (_data_->self);
#line 4 "test-shared-query.vala"
	g_slice_free (TestAppTestQueryAsyncData, _data_);
#line 629 "test-shared-query.c"
}


static void
test_app_test_query_async (TestApp* self,
                           GAsyncReadyCallback _callback_,
                           gpointer _user_data_)
{
	TestAppTestQueryAsyncData* _data_;
	TestApp* _tmp0_;
#line 4 "test-shared-query.vala"
	_data_ = g_slice_new0 (TestAppTestQueryAsyncData);
#line 4 "test-shared-query.vala"
	_data_->_async_result = g_task_new (G_OBJECT (self), NULL, _callback_, _user_data_);
#line 4 "test-shared-query.vala"
	g_task_set_task_data (_data_->_async_result, _data_, test_app_test_query_async_data_free);
#line 4 "test-shared-query.vala"
	_tmp0_ = _g_object_ref0 (self);
#line 4 "test-shared-query.vala"
	_data_->self = _tmp0_;
#line 4 "test-shared-query.vala"
	test_app_test_query_async_co (_data_);
#line 652 "test-shared-query.c"
}


static void
test_app_test_query_finish (TestApp* self,
                            GAsyncResult* _res_)
{
	TestAppTestQueryAsyncData* _data_;
#line 4 "test-shared-query.vala"
	_data_ = g_task_propagate_pointer (G_TASK (_res_), NULL);
#line 663 "test-shared-query.c"
}


static void
test_app_test_query_async_ready (GObject* source_object,
                                 GAsyncResult* _res_,
                                 gpointer _user_data_)
{
	TestAppTestQueryAsyncData* _data_;
#line 92 "test-shared-query.vala"
	_data_ = _user_data_;
#line 92 "test-shared-query.vala"
	_data_->_source_object_ = source_object;
#line 92 "test-shared-query.vala"
	_data_->_res_ = _res_;
#line 92 "test-shared-query.vala"
	test_app_test_query_async_co (_data_);
#line 681 "test-shared-query.c"
}


static gboolean
test_app_test_query_async_co (TestAppTestQueryAsyncData* _data_)
{
#line 87 "test-shared-query.vala"
	switch (_data_->_state_) {
#line 87 "test-shared-query.vala"
		case 0:
#line 692 "test-shared-query.c"
		goto _state_0;
#line 87 "test-shared-query.vala"
		case 1:
#line 696 "test-shared-query.c"
		goto _state_1;
		default:
#line 87 "test-shared-query.vala"
		g_assert_not_reached ();
#line 701 "test-shared-query.c"
	}
	_state_0:
#line 90 "test-shared-query.vala"
	g_print ("Async test\n");
#line 706 "test-shared-query.c"
	{
#line 92 "test-shared-query.vala"
		_data_->_tmp1_ = _data_->self->priv->con;
#line 92 "test-shared-query.vala"
		_data_->_state_ = 1;
#line 92 "test-shared-query.vala"
		tracker_sparql_connection_query_async (_data_->_tmp1_, "SELECT ?u WHERE { ?u a rdfs:Class }", NULL, test_app_test_query_async_ready, _data_);
#line 92 "test-shared-query.vala"
		return FALSE;
#line 716 "test-shared-query.c"
		_state_1:
#line 92 "test-shared-query.vala"
		_data_->_tmp2_ = tracker_sparql_connection_query_finish (_data_->_tmp1_, _data_->_res_, &_data_->_inner_error_);
#line 92 "test-shared-query.vala"
		_data_->_tmp0_ = _data_->_tmp2_;
#line 92 "test-shared-query.vala"
		if (G_UNLIKELY (_data_->_inner_error_ != NULL)) {
#line 724 "test-shared-query.c"
			goto __catch2_g_error;
		}
#line 92 "test-shared-query.vala"
		_data_->_tmp3_ = _data_->_tmp0_;
#line 92 "test-shared-query.vala"
		_data_->_tmp0_ = NULL;
#line 92 "test-shared-query.vala"
		_g_object_unref0 (_data_->cursor);
#line 92 "test-shared-query.vala"
		_data_->cursor = _data_->_tmp3_;
#line 91 "test-shared-query.vala"
		_g_object_unref0 (_data_->_tmp0_);
#line 737 "test-shared-query.c"
	}
	goto __finally2;
	__catch2_g_error:
	{
#line 91 "test-shared-query.vala"
		_data_->e = _data_->_inner_error_;
#line 91 "test-shared-query.vala"
		_data_->_inner_error_ = NULL;
#line 94 "test-shared-query.vala"
		_data_->_tmp4_ = _data_->e;
#line 94 "test-shared-query.vala"
		_data_->_tmp5_ = _data_->_tmp4_->message;
#line 94 "test-shared-query.vala"
		g_warning ("test-shared-query.vala:94: Couldn't perform query: %s", _data_->_tmp5_);
#line 95 "test-shared-query.vala"
		_data_->self->priv->res = -1;
#line 96 "test-shared-query.vala"
		_g_error_free0 (_data_->e);
#line 96 "test-shared-query.vala"
		_g_object_unref0 (_data_->cursor);
#line 96 "test-shared-query.vala"
		g_task_return_pointer (_data_->_async_result, _data_, NULL);
#line 96 "test-shared-query.vala"
		if (_data_->_state_ != 0) {
#line 96 "test-shared-query.vala"
			while (g_task_get_completed (_data_->_async_result) != TRUE) {
#line 96 "test-shared-query.vala"
				g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
#line 766 "test-shared-query.c"
			}
		}
#line 96 "test-shared-query.vala"
		g_object_unref (_data_->_async_result);
#line 96 "test-shared-query.vala"
		return FALSE;
#line 773 "test-shared-query.c"
	}
	__finally2:
#line 91 "test-shared-query.vala"
	if (G_UNLIKELY (_data_->_inner_error_ != NULL)) {
#line 91 "test-shared-query.vala"
		_g_object_unref0 (_data_->cursor);
#line 91 "test-shared-query.vala"
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, _data_->_inner_error_->message, g_quark_to_string (_data_->_inner_error_->domain), _data_->_inner_error_->code);
#line 91 "test-shared-query.vala"
		g_clear_error (&_data_->_inner_error_);
#line 91 "test-shared-query.vala"
		g_object_unref (_data_->_async_result);
#line 91 "test-shared-query.vala"
		return FALSE;
#line 788 "test-shared-query.c"
	}
#line 99 "test-shared-query.vala"
	_data_->_tmp6_ = _data_->cursor;
#line 99 "test-shared-query.vala"
	_data_->self->priv->res = test_app_iter_cursor (_data_->self, _data_->_tmp6_);
#line 101 "test-shared-query.vala"
	_data_->_tmp7_ = _data_->self->priv->res;
#line 101 "test-shared-query.vala"
	if (_data_->_tmp7_ == -1) {
#line 102 "test-shared-query.vala"
		_g_object_unref0 (_data_->cursor);
#line 102 "test-shared-query.vala"
		g_task_return_pointer (_data_->_async_result, _data_, NULL);
#line 102 "test-shared-query.vala"
		if (_data_->_state_ != 0) {
#line 102 "test-shared-query.vala"
			while (g_task_get_completed (_data_->_async_result) != TRUE) {
#line 102 "test-shared-query.vala"
				g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
#line 808 "test-shared-query.c"
			}
		}
#line 102 "test-shared-query.vala"
		g_object_unref (_data_->_async_result);
#line 102 "test-shared-query.vala"
		return FALSE;
#line 815 "test-shared-query.c"
	}
#line 104 "test-shared-query.vala"
	g_print ("\nRewinding\n");
#line 105 "test-shared-query.vala"
	_data_->_tmp8_ = _data_->cursor;
#line 105 "test-shared-query.vala"
	tracker_sparql_cursor_rewind (_data_->_tmp8_);
#line 107 "test-shared-query.vala"
	g_print ("\nSecond run\n");
#line 108 "test-shared-query.vala"
	_data_->_tmp9_ = _data_->cursor;
#line 108 "test-shared-query.vala"
	_data_->self->priv->res = test_app_iter_cursor (_data_->self, _data_->_tmp9_);
#line 87 "test-shared-query.vala"
	_g_object_unref0 (_data_->cursor);
#line 87 "test-shared-query.vala"
	g_task_return_pointer (_data_->_async_result, _data_, NULL);
#line 87 "test-shared-query.vala"
	if (_data_->_state_ != 0) {
#line 87 "test-shared-query.vala"
		while (g_task_get_completed (_data_->_async_result) != TRUE) {
#line 87 "test-shared-query.vala"
			g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
#line 839 "test-shared-query.c"
		}
	}
#line 87 "test-shared-query.vala"
	g_object_unref (_data_->_async_result);
#line 87 "test-shared-query.vala"
	return FALSE;
#line 846 "test-shared-query.c"
}


static void
test_app_do_sync_tests (TestApp* self)
{
#line 111 "test-shared-query.vala"
	g_return_if_fail (self != NULL);
#line 112 "test-shared-query.vala"
	test_app_test_query (self);
#line 857 "test-shared-query.c"
}


static void
test_app_do_async_tests_data_free (gpointer _data)
{
	TestAppDoAsyncTestsData* _data_;
	_data_ = _data;
#line 4 "test-shared-query.vala"
	_g_object_unref0 (_data_->self);
#line 4 "test-shared-query.vala"
	g_slice_free (TestAppDoAsyncTestsData, _data_);
#line 870 "test-shared-query.c"
}


static void
test_app_do_async_tests (TestApp* self,
                         GAsyncReadyCallback _callback_,
                         gpointer _user_data_)
{
	TestAppDoAsyncTestsData* _data_;
	TestApp* _tmp0_;
#line 4 "test-shared-query.vala"
	_data_ = g_slice_new0 (TestAppDoAsyncTestsData);
#line 4 "test-shared-query.vala"
	_data_->_async_result = g_task_new (G_OBJECT (self), NULL, _callback_, _user_data_);
#line 4 "test-shared-query.vala"
	g_task_set_task_data (_data_->_async_result, _data_, test_app_do_async_tests_data_free);
#line 4 "test-shared-query.vala"
	_tmp0_ = _g_object_ref0 (self);
#line 4 "test-shared-query.vala"
	_data_->self = _tmp0_;
#line 4 "test-shared-query.vala"
	test_app_do_async_tests_co (_data_);
#line 893 "test-shared-query.c"
}


static void
test_app_do_async_tests_finish (TestApp* self,
                                GAsyncResult* _res_)
{
	TestAppDoAsyncTestsData* _data_;
#line 4 "test-shared-query.vala"
	_data_ = g_task_propagate_pointer (G_TASK (_res_), NULL);
#line 904 "test-shared-query.c"
}


static void
test_app_do_async_tests_ready (GObject* source_object,
                               GAsyncResult* _res_,
                               gpointer _user_data_)
{
	TestAppDoAsyncTestsData* _data_;
#line 116 "test-shared-query.vala"
	_data_ = _user_data_;
#line 116 "test-shared-query.vala"
	_data_->_source_object_ = source_object;
#line 116 "test-shared-query.vala"
	_data_->_res_ = _res_;
#line 116 "test-shared-query.vala"
	test_app_do_async_tests_co (_data_);
#line 922 "test-shared-query.c"
}


static gboolean
test_app_do_async_tests_co (TestAppDoAsyncTestsData* _data_)
{
#line 115 "test-shared-query.vala"
	switch (_data_->_state_) {
#line 115 "test-shared-query.vala"
		case 0:
#line 933 "test-shared-query.c"
		goto _state_0;
#line 115 "test-shared-query.vala"
		case 1:
#line 937 "test-shared-query.c"
		goto _state_1;
		default:
#line 115 "test-shared-query.vala"
		g_assert_not_reached ();
#line 942 "test-shared-query.c"
	}
	_state_0:
#line 116 "test-shared-query.vala"
	_data_->_state_ = 1;
#line 116 "test-shared-query.vala"
	test_app_test_query_async (_data_->self, test_app_do_async_tests_ready, _data_);
#line 116 "test-shared-query.vala"
	return FALSE;
#line 951 "test-shared-query.c"
	_state_1:
#line 116 "test-shared-query.vala"
	test_app_test_query_finish (_data_->self, _data_->_res_);
#line 118 "test-shared-query.vala"
	g_print ("Async tests done, now I can quit the mainloop\n");
#line 119 "test-shared-query.vala"
	_data_->_tmp0_ = _data_->self->priv->loop;
#line 119 "test-shared-query.vala"
	g_main_loop_quit (_data_->_tmp0_);
#line 115 "test-shared-query.vala"
	g_task_return_pointer (_data_->_async_result, _data_, NULL);
#line 115 "test-shared-query.vala"
	if (_data_->_state_ != 0) {
#line 115 "test-shared-query.vala"
		while (g_task_get_completed (_data_->_async_result) != TRUE) {
#line 115 "test-shared-query.vala"
			g_main_context_iteration (g_task_get_context (_data_->_async_result), TRUE);
#line 969 "test-shared-query.c"
		}
	}
#line 115 "test-shared-query.vala"
	g_object_unref (_data_->_async_result);
#line 115 "test-shared-query.vala"
	return FALSE;
#line 976 "test-shared-query.c"
}


static gboolean
test_app_in_mainloop (TestApp* self)
{
	gboolean result = FALSE;
#line 122 "test-shared-query.vala"
	g_return_val_if_fail (self != NULL, FALSE);
#line 124 "test-shared-query.vala"
	test_app_do_sync_tests (self);
#line 125 "test-shared-query.vala"
	test_app_do_async_tests (self, NULL, NULL);
#line 127 "test-shared-query.vala"
	result = FALSE;
#line 127 "test-shared-query.vala"
	return result;
#line 994 "test-shared-query.c"
}


static gboolean
_test_app_in_mainloop_gsource_func (gpointer self)
{
	gboolean result;
	result = test_app_in_mainloop ((TestApp*) self);
#line 133 "test-shared-query.vala"
	return result;
#line 1005 "test-shared-query.c"
}


gint
test_app_run (TestApp* self)
{
	gint result = 0;
	GMainLoop* _tmp0_;
	GMainLoop* _tmp1_;
	gint _tmp2_;
#line 130 "test-shared-query.vala"
	g_return_val_if_fail (self != NULL, 0);
#line 131 "test-shared-query.vala"
	_tmp0_ = g_main_loop_new (NULL, FALSE);
#line 131 "test-shared-query.vala"
	_g_main_loop_unref0 (self->priv->loop);
#line 131 "test-shared-query.vala"
	self->priv->loop = _tmp0_;
#line 133 "test-shared-query.vala"
	g_idle_add_full (G_PRIORITY_DEFAULT_IDLE, _test_app_in_mainloop_gsource_func, g_object_ref (self), g_object_unref);
#line 135 "test-shared-query.vala"
	_tmp1_ = self->priv->loop;
#line 135 "test-shared-query.vala"
	g_main_loop_run (_tmp1_);
#line 137 "test-shared-query.vala"
	_tmp2_ = self->priv->res;
#line 137 "test-shared-query.vala"
	result = _tmp2_;
#line 137 "test-shared-query.vala"
	return result;
#line 1036 "test-shared-query.c"
}


static void
test_app_class_init (TestAppClass * klass)
{
#line 4 "test-shared-query.vala"
	test_app_parent_class = g_type_class_peek_parent (klass);
#line 4 "test-shared-query.vala"
	g_type_class_adjust_private_offset (klass, &TestApp_private_offset);
#line 4 "test-shared-query.vala"
	G_OBJECT_CLASS (klass)->finalize = test_app_finalize;
#line 1049 "test-shared-query.c"
}


static void
test_app_instance_init (TestApp * self)
{
#line 4 "test-shared-query.vala"
	self->priv = test_app_get_instance_private (self);
#line 7 "test-shared-query.vala"
	self->priv->res = 0;
#line 1060 "test-shared-query.c"
}


static void
test_app_finalize (GObject * obj)
{
	TestApp * self;
#line 4 "test-shared-query.vala"
	self = G_TYPE_CHECK_INSTANCE_CAST (obj, TYPE_TEST_APP, TestApp);
#line 5 "test-shared-query.vala"
	_g_main_loop_unref0 (self->priv->loop);
#line 6 "test-shared-query.vala"
	_g_object_unref0 (self->priv->con);
#line 4 "test-shared-query.vala"
	G_OBJECT_CLASS (test_app_parent_class)->finalize (obj);
#line 1076 "test-shared-query.c"
}


GType
test_app_get_type (void)
{
	static volatile gsize test_app_type_id__volatile = 0;
	if (g_once_init_enter (&test_app_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (TestAppClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) test_app_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (TestApp), 0, (GInstanceInitFunc) test_app_instance_init, NULL };
		GType test_app_type_id;
		test_app_type_id = g_type_register_static (G_TYPE_OBJECT, "TestApp", &g_define_type_info, 0);
		TestApp_private_offset = g_type_add_instance_private (test_app_type_id, sizeof (TestAppPrivate));
		g_once_init_leave (&test_app_type_id__volatile, test_app_type_id);
	}
	return test_app_type_id__volatile;
}



